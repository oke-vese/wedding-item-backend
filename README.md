# Wedding Items

An application where vendors can put up wedding items for rent and customers can see what they need for their 
wedding and contact the vendor to progress to purchase.

##Getting Started

Get a local copy of the project you would have to `cd` into each top level folder and `npm install` e.g

```sh
$ cd wedding-item-client
$ npm install 
```


##Prerequisites
 You need the lastest versions of:
 
 - firebase cli tools
 - angular-cli
 
 NB: Ignore the warning on Firebase for npm version 6. 
 
 
 
 
 
##Built With
 
 - Node.js
 - Firebase
 - Angular