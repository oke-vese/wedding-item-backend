const express = require('express');
const admin = require('firebase-admin');
const googleStorage = require('@google-cloud/storage');
const Multer = require('multer');

const config = require('../../config/index').config;

const router = express.Router();

const storage = googleStorage({
  projectId: config.projectId,
  credentials: require('../../service_account/service-account.json')
});
const bucket = storage.bucket("https://console.cloud.google.com/storage/browser/staging.weddingitems-6f4b6.appspot.com?project=weddingitems-6f4b6&folder&organizationId");

const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024 // =< 5MB
  }
});

// TODO: Write function to test file type for only images

// Upload image file to Google storage
const uploadImageToStorage = (file) => {
  let promise = new Promise((resolve, reject) => {
    if (!file) {
      reject('No image file');
    }
    let newFileName = `${file.originalName}_${Date.now()}`;
    let fileUpload = bucket.file(newFileName);

    const blobStream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype
      }
    });

    blobStream.on('error', (error) => {
      reject('Something went wrong! Unable to upload');
    });
    blobStream.on('finish', () => {
      // The public URL can be used to directly access the file via HTTP
      const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`);
      resolve(url);
    });
    blobStream.end(file.buffer);
  });
  return promise;
}

router.post('/upload', multer.single('file'), (req, res) => {
  console.log('Uploading image...');

  let file = req.file;
  if(file) {
    uploadImageToStorage(file).then((success) => {
      res.status(200).send({
        status: 'success'
      });
    }).catch((error) => {
      console.error(error);
    });
  }
});