const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')({ origin: true });

const app = express();
const router = express.Router();

const api = require('./routes/index');
const user = require('./routes/user');
const admin = require('./routes/admin');

app.use(cors);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
router.use('/functions', api);
router.use('/user', user);
router.use('/admin', admin);
app.use('/api/v1', router);

exports.app = functions.https.onRequest(app);


