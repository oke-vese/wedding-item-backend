
const express = require('express');
const router = express.Router();
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const serviceAccount = require('../service_account/service-account.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://weddingitems-6f4b6.firebaseio.com'
});

const db = admin.database();
const ref = db.ref("/");

// TODO: refactor to use error middleware. Use eslint for error checcking in deploy

/**
 * Adds a review
 * @param {number} year - Year of date
 * @param {number} month - Month of date
 * @param {number} day - Day of date
 * @param {number} id - id of review
 * @param {string} review - review 
 * @param {string} name - name of review poster passed in request body
 */

router.post('/review/:reviewid', (req, res) => {
  if (!req.params.reviewid) {
    sendResponse(res, 404, {
      "message": "Not found, reviewid is required"
    });
    return;
  }  
  if (isNaN(req.params.reviewid)) {
    sendResponse(res, 412, { "message": "reviewid must be a number" });
  }
  const reviewid = req.params.reviewid;
  const reviewRef = ref.child('reviews/').child(reviewid);
  const date = new Date(req.body.year, req.body.month, req.body.day);

  reviewRef.set({
    "id": req.params.reviewid,
    "name": req.body.name,
    "review": req.body.review,
    "date": date.toDateString()
  });
  sendResponse(res, 201, { "message": "success" });
});



// Get full list of reviews
router.get('/reviews', (req, res) => {
  db.ref('reviews')
    .once('value')
    .then((snapshot) => {
      const reviews = snapshot.val();
      sendResponse(res, 200, reviews);
    })
    .catch((err) => {
      sendResponse(res, 500, { "message": "An error occurred" });
    });
});



// Delete a review
router.delete('/review/:reviewid', (req, res) => {
  if (!req.params.reviewid) {
    sendResponse(res, 404, {
      "message": "Not found, reviewid is required"
    });
    return;
  }  
  if (isNaN(req.params.reviewid)) {
    sendResponse(res, 412, { "message": "reviewid must be a number" });
  }
  const reviewid = req.params.reviewid;
  db.ref('reviews')
    .child(reviewid)
    .remove();
  sendResponse(res, 202, { "message": "Deleted" });
});



/**
 * Adds an item
 * @param {number} quantity - Quantity of items
 * @param {string} category - Item's category name
 * @param {number} price - Price of the item
 * @param {string} description - Item description
 * @param {string} itemid - Item id
 */

router.post('/items/:itemid', (req, res) => {
  if (!req.params.itemid) {
    sendResponse(res, 404, {
      "message": "Not found, itemid is required"
    });
    return;
  }  
  if (!isNaN(req.params.itemid)) {
    sendResponse(res, 412, { "message": "itemid must be a string" });
  }
  const itemid = req.params.itemid;
  const itemRef = ref.child('items/').child(itemid);
  itemRef.set({
    "id": req.body.id,
    "description": req.body.description,
    "price": req.body.price,
    "quantity": req.body.quantity,
    "category": req.body.category
  });
  sendResponse(res, 201, { "message": "successful" });
});


/**
 * Vendor updates an item
 * @param {number} quantity - Parameter passed in request body
 * @param {string} category - Parameter passed in request body
 * @param {number} price - Parameter passed in request body
 * @param {string} description - Parameter passed in request body
 * @param {number} itemid - Parameter passed in request body
 * @param {string} name - Parameter passed in request body
 */

 router.put('/items/:itemid', (req, res) => {
   if (req.params && req.params.itemid) {
     const itemid = req.params.itemid;
     const itemRef = db.ref('items')
      .child(itemid)
      .update({
        "description": req.body.description
      });
   }
   else {
    sendResponse(res, 404, { "message": "No itemid in request" });  
   }
 })



/**
 * Vendor deletes an item
 * @param {number} itemid - Parameter passed in request body
 */

router.delete('/items/:itemid', (req, res) => {
  const itemid = req.params.itemid;
  db.ref('items').child(itemid).remove();
  sendResponse(res, 202, { "message": "deleted" });
});


// Get full list of items
router.get('/items', (req, res) => {
  db.ref('items')
    .once('value')
    .then((snapshot) => {
      const items = snapshot.val();
      sendResponse(res, 200, items);
    })
    .catch((err) => {
      sendResponse(res, 500, { "message": "An error occurred" });
    });
});



/**
 * Adds a category
 * @param {string} categoryid - Name of category passed in request params
 * @param {string} description - Short description of item,
 * @param {string} itemid - Name of item added to category
 */

router.post('/category/:categoryid/:itemid', (req, res) => {
  if (req.params && req.params.categoryid) {
    const categoryid = req.params.categoryid;
    const itemid = req.params.itemid;
    const categoryRef = ref.child('category').child(categoryid).child(itemid);
    categoryRef.set({
      "category": req.params.categoryid,
      "name": req.body.name,
      "description": req.body.description,
      "price": req.body.price,
      "quantity": req.body.quantity,
      "id": req.body.id
    });
    sendResponse(res, 201, { "message": "successful" });
  }
  else {
    sendResponse(res, 404, { "message": "No categoryid in request" });
  }
});


// Get all categories
router.get('/category', (req, res) => {
  db.ref('category')
    .once('value')
    .then((snapshot) => {
      const categories = snapshot.val();
      sendResponse(res, 200, categories);
    })
    .catch((err) => {
      sendResponse(res, 500, { "message": "An error occurred" });
    });
});

// Get a category
router.get('/category/:categoryid', (req, res) => {
  const categoryid = req.params.categoryid;
  db.ref('category')
    .child(categoryid)
    .once('value')
    .then((snapshot) => {
      const category = snapshot.val();
      sendResponse(res, 200, category);
    })
    .catch(err => {
      sendResponse(res, 500, {message: "An error occured" });
    });
});



// Delete category
router.delete('/category/:categoryid', (req, res) => {
  const categoryid = req.params.categoryid;
  db.ref('category').child(categoryid).remove();
  res.status(202).send('Deleted');
});



// Add to cart. 
router.post('/cart/:itemid', (req, res) => {
  const itemid = req.params.itemid;
  const savedItemRef = ref.child('cart/').child(itemid);
  savedItemRef.set({
    "id": req.params.itemid,
    "price": req.body.price,
    "name": req.body.name
  });
  sendResponse(res, 201, { "message": "successful" });
});


// Get list of items in cart
router.get('/cart', (req, res) => {
  db.ref('cart')
    .once('value')
    .then((snapshot) => {
      const savedItems = snapshot.val();
      res.status(200).send(savedItems);
    })
    .catch(err => {
      res.status(500).send('Error occured', err);
    });
});


// Remove from cart
router.delete('/cart/:itemid', (req, res) => {
  const itemid = req.params.itemid;
  db.ref('cart').child(itemid).remove();
  res.status(202).send('Deleted');
});

const sendResponse = (res, status, content) => {
  res
    .status(status)
    .send(content);
} 


module.exports = router;

