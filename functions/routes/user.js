
const express = require('express');
const router = express.Router();
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const db = admin.database();
const ref = db.ref("/");


// Function is triggered on user creation
exports.saveUserInDatabase = functions.auth.user().onCreate(event => {
  const uid = event.data.uid;
  const user = event.data;
  const email = event.data.email;
  const phoneNumber = event.data.phoneNumber;
  const displayName = event.data.displayName;
  return customerRef = ref.child('customers').child(uid);
    customerRef.set({
      "email": email,
      "phoneNumber": phoneNumber,
      "displayName": displayName
    });
});

module.exports = router;