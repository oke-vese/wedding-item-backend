const express = require('express');
const router = express.Router();
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const db = admin.database();
const ref = db.ref("/");



router.post('/create/admin', (req, res) => {
  admin.auth().createUser({
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    password: req.body.password,
    displayName: req.body.displayName
  })
    .then(userRecord => {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log(`Successfully created new user: ${userRecord.uid}`);
      const userRef = ref.child('users').child(userRecord.uid);
      userRef.set({
        "email": req.body.email,
        "phoneNumber": req.body.phoneNumber,
        "displayName": req.body.displayName,
        "address": req.body.address
      })
      res.status(201).send('Created!');
    })
    .catch(err => {
      console.log(`Error creating new user: ${err}`);
      res.status(400).send({
        message: `Error while creating new account, with error: ${err}`
      });
    });
});

module.exports = router;